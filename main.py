import uuid
from datetime import datetime, timezone

import uvicorn
from fastapi import FastAPI, UploadFile
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
import shutil

from settings import BASE_URL, STORAGE_FOLDER


class UploadFileResponse(BaseModel):
    uploaded_at: int
    download_path: str
    request_duration: str


app = FastAPI()
app.mount(f"/{STORAGE_FOLDER}", StaticFiles(directory=STORAGE_FOLDER), name=STORAGE_FOLDER)
app.mount(f"/static", StaticFiles(directory='static'), name='static')


@app.get("/")
async def hello():
    return "Server running"


@app.post("/upload")
async def upload_file_to_storage(file: UploadFile, request_ts: str) -> UploadFileResponse:
    filename = file.filename
    extension = filename.split('.')[-1]
    filename = filename.split('.')[:-1]
    filename = f'{"".join(filename)}_{uuid.uuid4().hex[:8]}.{extension}'
    path = f'{STORAGE_FOLDER}/{filename}'

    source = file.file
    with open(path, mode='wb') as f:
        shutil.copyfileobj(source, f)

    download_path = BASE_URL + path
    uploaded_at = datetime.now(timezone.utc).timestamp()
    request_duration = uploaded_at - float(request_ts)
    request_duration = f'{round(request_duration, 3)} seconds'
    uploaded_at = round(uploaded_at*1000)

    data_dict = UploadFileResponse(
        uploaded_at=uploaded_at,
        download_path=download_path,
        request_duration=request_duration
    )
    return data_dict

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
