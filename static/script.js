const uploadForm = document.querySelector(".upload");
const info = document.querySelector(".info");

uploadForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  info.replaceChildren();
  const file = e.target[0].files[0];
  const formData = new FormData();
  formData.append("file", file);

  const response = await fetch(
    `http://13.52.247.5/upload?request_ts=${Date.now() / 1000}`,
    {
      method: "POST",
      mode: "no-cors",
      body: formData,
    }
  );
  const data = await response.json();

  uploadForm.reset();
  const { uploaded_at, download_path, request_duration } = data;

  const dateObj = new Date(uploaded_at);
  const dateStr = dateObj.toString();
  const month = dateObj.getMonth() + 1;
  const date =
    dateStr.slice(8, 10) +
    "." +
    `${month < 10 ? "0" + month : month}` +
    "." +
    dateObj.getFullYear() +
    " at " +
    dateStr.slice(16, 21);

  const html = `
  <div>
  <p><b>Open/Download:</b> </p><a href="${download_path}"target="_blank">${download_path}</a>
  <p><b>Uploaded:</b> ${date}</p>
  <p><b>Request duration:</b> ${request_duration}</p>
  </div>
  `;
  info.innerHTML = html;
});
